
#### 流程编辑器的整合
流程编辑器下载[工作流流程编辑器mdp-workflow-modeler-ui-web](https://gitee.com/maimengcloud/mdp-workflow-modeler-ui-web)，

流程编辑器的整合分两种使用场景：  
1.本地开发调试运行
将mdp-workflow-modeler-ui-web/modeler-ui整个目录拷贝到
mdp-lcode-ui-web/static/下
并配置页面代理
```js 
      '/workflow/m1/modeler-ui/': {
        target: 'http://localhost:8015',
        changeOrigin: true,
        pathRewrite: {
          '^/workflow/m1/modeler-ui/': '/modeler-ui/'
        }
      },
```
还需要配置工作流api请求代理
假设mdp-workflow 本地启动并监听 7080 端口
```js 
      '/api/m1/workflow': {
        target: 'http://localhost:7080',
        changeOrigin: true,
        pathRewrite: {
          '^/api/m1/workflow': '/'
        }
      },
```
此代理实现将/api/m1/workflow/开头的请求转发到/http://localhost:7080/下

2.服务器上运行
将mdp-workflow-modeler-ui-web/modeler-ui整个目录拷贝到服务器上，如果是nginx,建议放在nginx/html/下
如果是nginx,需要配置请求地址/workflow/m1/modeler-ui/->到nginx/html/modeler-ui/的映射关系

