/*
 * Copyright 2005-2015 Alfresco Software, Ltd. All rights reserved.
 * License rights for this program may be obtained from Alfresco Software, Ltd.
 * pursuant to a written agreement and any use of this program without such an
 * agreement is prohibited.
 */
'use strict';

var ACTIVITI = ACTIVITI || {};

ACTIVITI.CONFIG = {
	'onPremise' : true,
	'contextRoot' : '/api/m1/workflow/workflow',
	'webContextRoot' : '/workflow/m1/editor',
	'sysName': 'MDP-多功能快速开发平台',
	'logoUrl': '',
	'version':'m1',
	'appName':'editor'
};
/*
 * Copyright 2005-2015 Alfresco Software, Ltd. All rights reserved.
 * License rights for this program may be obtained from Alfresco Software, Ltd.
 * pursuant to a written agreement and any use of this program without such an
 * agreement is prohibited.
 */
//
// DEV ENV OVERRIDES
//

